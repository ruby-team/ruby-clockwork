require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  # This test makes autopkgtest hangs, skipping it for now
  skipped_test = ['test/signal_test.rb', 'test/manager_test.rb', 'test/clockwork_test.rb']
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb'] - skipped_test
end
