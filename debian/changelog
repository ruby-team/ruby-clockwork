ruby-clockwork (3.0.1-2) unstable; urgency=medium

  * d/ruby-tests.rake: skip tests failing with ruby 3.3
  * Declare compliance with Debian Policy 4.7.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 16 Dec 2024 19:59:04 -0300

ruby-clockwork (3.0.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-activesupport.
    + ruby-clockwork: Drop versioned constraint on ruby-activesupport in
      Depends.
  * Update standards version to 4.6.1, no changes needed.

  [ Cédric Boutillier ]
  * New upstream version 3.0.1
  * Refresh patches
  * Add build-dependency on ruby
  * Remove X?-Ruby-Versions fields from d/control
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 07 Feb 2023 15:13:17 +0100

ruby-clockwork (2.0.4-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Fix field name case in debian/control (Rules-requires-root =>
    Rules-Requires-Root).

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package
  * Require ostruct in test (Closes: #1005446)
  * Use ${ruby:Depends} for dependencies
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * upgrade d/watch to v4
  * Use gem install layout

 -- Cédric Boutillier <boutil@debian.org>  Tue, 22 Feb 2022 15:23:13 +0100

ruby-clockwork (2.0.4-1) unstable; urgency=medium

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Lucas Kanashiro ]
  * New upstream version 2.0.4
  * Refresh patch
  * Bump debhelper compatibility level to 12
  * d/control: do not require root to build
  * Add myself to Uploaders list
  * Declare compliance with Debian Policy 4.5.0

 -- Lucas Kanashiro <lucas.kanashiro@canonical.com>  Thu, 06 Feb 2020 09:38:09 -0300

ruby-clockwork (2.0.3-4) unstable; urgency=medium

  * Team upload.
  * Add patch to not make relative requires in test files
  * Skip specific test that makes autopkgtest hangs

 -- Lucas Kanashiro <kanashiro@debian.org>  Tue, 12 Feb 2019 09:02:09 -0200

ruby-clockwork (2.0.3-3) unstable; urgency=medium

  * Apply drop-git-in-gemspec.patch: fix failed tests in debci.
  * Update upstream url.
  * debian/watch: fetch from upstream's github instead.
  * Update copyright file.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Mon, 11 Feb 2019 17:02:45 +0800

ruby-clockwork (2.0.3-2) unstable; urgency=medium

  * Bump Standard-version to 4.3.0.
  * Upload to unstable.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 08 Feb 2019 13:17:13 +0800

ruby-clockwork (2.0.3-1) experimental; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Andrew Lee (李健秋) ]
  * Apply disable-sync_performer_test.patch: disable specific test that
    randomly fails on havey load machine. (Closes: #846020)

  [ Lucas Kanashiro ]
  * New upstream version 2.0.3
  * Bump debhelper compatibility level to 11
  * Declare complaince with Debian Policy 4.2.0
  * Update VCS urls to point to salsa
  * debian/copyright: use secure url in Format field (https://)
  * debian/copyright: reorganize paragraphs
  * debian/copyright: use secure url in Source field (https://)
  * Remove patch fix_test_with_minitest.patch applied by upstream
  * Update patch that disable sync_performer_test
  * Add version constraint to ruby-activesupport (>= 2:5)
  * debian/watch: use secure url (https://)

 -- Lucas Kanashiro <lucas.kanashiro@collabora.co.uk>  Thu, 23 Aug 2018 14:08:15 -0300

ruby-clockwork (1.2.0-3) unstable; urgency=medium

  * Team Upload.
  * Add patch fix_test_with_minitest.patch (Closes: #816480)
  * Update packaging using dh-make-ruby -w
    - Bump Standards-Versio to 3.9.7 (no changes needed)
    - Set debhelper compatibility level to 9
    - use https:// in Vcs-* fields

 -- Thiago Ribeiro <thiago@pencillabs.com>  Thu, 03 Mar 2016 12:21:42 -0300

ruby-clockwork (1.2.0-2) unstable; urgency=medium

  * debian/control: set correct build-deps and depends.
  * debian/copyright: add copyright year.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 13 Jan 2016 03:53:05 +0800

ruby-clockwork (1.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #797139)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 28 Aug 2015 12:50:59 +0800
